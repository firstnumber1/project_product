package registry

import (
	"helloworld/handler"
	"net/http"

	"gitlab.com/ditp.thaitrade/servicex/server"
)

func WebAPIRegister() []*server.APIRegistry {
	return []*server.APIRegistry{
		{
			URL:                             "/product/api/v1/show",
			Handler:                         handler.GetProductHandler,
			Method:                          http.MethodGet,
			SkipDefaultServerAPIMiddleWares: true,
		},
		{
			URL:                             "/product/api/v1/ShowOrder",
			Handler:                         handler.ShowOrderHandler,
			Method:                          http.MethodPost,
			SkipDefaultServerAPIMiddleWares: true,
		},
		{
			URL:                             "/product/api/v1/AddOrder",
			Handler:                         handler.AddOrderHandler,
			Method:                          http.MethodPost,
			SkipDefaultServerAPIMiddleWares: true,
		},
	}
}

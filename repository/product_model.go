package repository

type ProductShowsRes struct {
	Product_Id     int    `json:"product_id"`
	Product_Name   string `json:"product_name"`
	Product_Type   string `json:"product_type"`
	Product_Detail string `json:"product_detail"`
}

type ShowOrderRes struct {
	Oder_Id    int `json:"order_id"`
	Product_Id int `json:"name"`
	Price      int `json:"price"`
	Quantity   int `json:"quantity"`
}

type AddOrderRes struct {
	User_Id    int `json:"user_id"`
	Product_Id int `json:"product_id"`
	Price      int `json:"price"`
	Quantity   int `json:"quantity"`
}

package repository

import (
	"database/sql"

	log "gitlab.com/ditp.thaitrade/enginex/echo_logrus"
	"gitlab.com/ditp.thaitrade/servicex/server"
)

const (
	defaultDBContextName = "PRODUCT"
)

type ProductsRepository struct {
	db *sql.DB
}

type Product struct {
	UserId int
}

type ProductsOption func(pd *ProductsRepository) error

func NewCondb(options ...ProductsOption) (*ProductsRepository, error) {
	repo := &ProductsRepository{}

	for _, opt := range options {
		if err := opt(repo); err != nil {
			return nil, err
		}
	}

	if repo.db == nil {
		conns := server.Instant().DBConnections()
		for contextName, conn := range conns {
			if contextName == defaultDBContextName {
				repo.db = conn
			}
		}
	}

	return repo, nil
}

func (ps *ProductsRepository) GetProduct() ([]*ProductShowsRes, error) {
	sqlCmd := `select product_id, product_name, product_type, product_detail
	from products`

	stmt, err := ps.db.Prepare(sqlCmd)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	responses := make([]*ProductShowsRes, 0)
	for rows.Next() {
		response := &ProductShowsRes{}
		if err := rows.Scan(
			&response.Product_Id,
			&response.Product_Name,
			&response.Product_Type,
			&response.Product_Detail,
		); err != nil {
			log.Errorf("GetProduct error : %s", err)
			return nil, err
		}
		responses = append(responses, response)
	}

	return responses, err
}

func (ps *ProductsRepository) AddOrderRepository(req *AddOrderRes) error {
	tx, err := ps.db.Begin()
	if err != nil {
		tx.Rollback()
		return err
	}

	sqlCmd := `INSERT INTO product_order(user_id)
	VALUES ($1)RETURNING order_id;`

	stmt, err := tx.Prepare(sqlCmd)
	if err != nil {
		tx.Rollback()
		return err
	}
	defer stmt.Close()

	rows, err := stmt.Query(req.User_Id)
	if err != nil {
		tx.Rollback()
		return err
	}

	var Order_id int
	for rows.Next() {
		if err := rows.Scan(
			&Order_id,
		); err != nil {
			log.Errorf("GetProduct error : %s", err)
			return err
		}
	}

	sqlCmd2 := `INSERT INTO include(order_id,product_id,price,quantity)
	VALUES ($1,$2,$3,$4);`

	stmt, err = tx.Prepare(sqlCmd2)
	if err != nil {
		tx.Rollback()
		return err
	}
	defer stmt.Close()

	rows, err = stmt.Query(Order_id, req.Product_Id, req.Price, req.Quantity)
	if err != nil {
		tx.Rollback()
		return err
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return err
	}
	return nil
}

func (ps *ProductsRepository) ShowOrderRepository(order_Id int) ([]*ShowOrderRes, error) {
	sqlCmd := `select a2.order_id,a2.product_id,a2.price,a2.quantity from product_order a1
left join include a2 on a1.order_id = a2.order_id
where a2.order_id = $1`

	stmt, err := ps.db.Prepare(sqlCmd)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query(order_Id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	responses := make([]*ShowOrderRes, 0)
	for rows.Next() {
		response := &ShowOrderRes{}
		if err := rows.Scan(
			&response.Oder_Id,
			&response.Product_Id,
			&response.Price,
			&response.Quantity,
		); err != nil {
			log.Errorf("GetProduct error : %s", err)
			return nil, err
		}
		responses = append(responses, response)
	}

	return responses, err
}

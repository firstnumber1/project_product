package services

import (
	"helloworld/repository"
)

type ProductService struct {
	repo *repository.ProductsRepository
}

type ProductOption func(ps *ProductService) error

func ProductRepoOpt(repo *repository.ProductsRepository) ProductOption {
	return func(p *ProductService) error {
		p.repo = repo

		return nil
	}
}

func NewCondb(options ...ProductOption) (*ProductService, error) {
	sv := &ProductService{}

	for _, opt := range options {
		if err := opt(sv); err != nil {
			return nil, err
		}
	}

	if sv.repo == nil {
		pRepo, err := repository.NewCondb()
		if err != nil {
			return nil, err
		}
		sv.repo = pRepo
	}

	return sv, nil
}

func (ps *ProductService) ProductShow() ([]*repository.ProductShowsRes, error) {

	model, err := ps.repo.GetProduct()
	if err != nil {
		return nil, err
	}
	return model, nil
}

func (ps *ProductService) AddOrder(req *repository.AddOrderRes) error {

	err := ps.repo.AddOrderRepository(req)
	if err != nil {
		return err
	}
	return nil
}

func (ps *ProductService) ShowOrder(order_Id int) ([]*repository.ShowOrderRes, error) {

	model, err := ps.repo.ShowOrderRepository(order_Id)
	if err != nil {
		return nil, err
	}
	return model, nil
}

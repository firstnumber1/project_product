package main

import (
	"helloworld/registry"

	log "gitlab.com/ditp.thaitrade/enginex/echo_logrus"

	"gitlab.com/ditp.thaitrade/servicex/server"
)

func main() {
	serverOps := []server.ServerOption{
		server.APIRegistryOpt(registry.WebAPIRegister()),
	}

	project, err := server.New(serverOps...)
	if err != nil {
		log.Fatalf("error start server: %s", err)
	}

	project.Start()
}

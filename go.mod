module helloworld

go 1.16

require (
	github.com/labstack/echo v3.3.10+incompatible
	gitlab.com/ditp.thaitrade/enginex v0.0.0-20210519041531-289cea16f9c5
	gitlab.com/ditp.thaitrade/servicex v0.0.0-20201104164959-bd77214e38cc
)

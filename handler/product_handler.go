package handler

import (
	"helloworld/repository"
	"helloworld/services"
	"net/http"

	"github.com/labstack/echo"
	log "gitlab.com/ditp.thaitrade/enginex/echo_logrus"
)

func GetProductHandler(ctx echo.Context) error {
	sv, err := services.NewCondb()
	if err != nil {
		log.Errorf("GetProductHandler error : %s", err)
		return ctx.String(http.StatusInternalServerError, "server error")
	}

	data, err := sv.ProductShow()
	if err != nil {
		log.Errorf("GetProductHandler error : %s", err)
		return ctx.String(http.StatusInternalServerError, "server error")
	}

	return ctx.JSON(http.StatusOK, data)
}

func ShowOrderHandler(ctx echo.Context) error {
	req := &repository.ShowOrderRes{}
	if err := ctx.Bind(req); err != nil {
		log.Errorf("AddOrderHandler error : %s", err)
		return ctx.String(http.StatusInternalServerError, "server error")
	}

	sv, err := services.NewCondb()
	if err != nil {
		log.Errorf("AddOrderHandler error : %s", err)
		return ctx.String(http.StatusInternalServerError, "server error")
	}

	data, err := sv.ShowOrder(req.Oder_Id)
	if err != nil {
		log.Errorf("ShowOrderHandler error : %s", err)
		return ctx.String(http.StatusInternalServerError, "server error")
	}

	return ctx.JSON(http.StatusOK, data)
}

func AddOrderHandler(ctx echo.Context) error {
	req := &repository.AddOrderRes{}
	if err := ctx.Bind(req); err != nil {
		log.Errorf("AddOrderHandler error : %s", err)
		return ctx.String(http.StatusInternalServerError, "server error")
	}

	sv, err := services.NewCondb()
	if err != nil {
		log.Errorf("AddOrderHandler error : %s", err)
		return ctx.String(http.StatusInternalServerError, "server error")
	}

	err = sv.AddOrder(req)
	if err != nil {
		log.Errorf("AddOrderHandler error : %s", err)
		return ctx.String(http.StatusInternalServerError, "server error")
	}

	return ctx.JSON(http.StatusOK, "success")
}
